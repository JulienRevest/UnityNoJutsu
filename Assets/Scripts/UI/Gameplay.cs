using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public Text Ammunition;
    public Text scoreText;
    public Slider Health;
    public Slider RollCooldown;
    public PlayerShooting playerShooting;
    public PlayerProperties playerProperties;
    public PlayerMovements playerMovements;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake()
    {
        //playerProperties = GetComponent<PlayerProperties>();
        //Health = GetComponent<Slider>();
        Ammunition.text = playerShooting.BulletCount.ToString() + " / " + playerShooting.MaxBullet.ToString();
        Health.maxValue = playerProperties.maxHealth;
        RollCooldown.maxValue = playerMovements.rollCooldown;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (playerShooting._shoot._shotCooldown > playerShooting._shoot.BulletInterval / 1000f)
        {
            Ammunition.text = "Reloading...";
        }
        else
        {
            Ammunition.text = playerShooting.BulletCount.ToString() + " / " + playerShooting.MaxBullet.ToString();
        }
        Health.value = playerProperties.currentHealth;
        RollCooldown.value = playerMovements.rollCooldownCounter;
    }
}
