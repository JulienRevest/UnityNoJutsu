using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SuperDuperSecret : MonoBehaviour
{
    public Text superSuperText;
    public GameObject plane;
    public GameObject amogus;
    public AudioSource rick;

    private bool isPlaying = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var textRotation = superSuperText.transform.rotation.eulerAngles;
        textRotation.z += Time.deltaTime * 150.0f;
        superSuperText.transform.rotation = Quaternion.Euler(textRotation);
        var amogusRotation = superSuperText.transform.rotation.eulerAngles;
        amogusRotation.x = 0;
        amogusRotation.z = 90;
        amogusRotation.y += Time.deltaTime * -200.0f;
        amogus.transform.rotation = Quaternion.Euler(amogusRotation);
    }

    public void OnTriggerEnter(Collider other)
    {
        plane.SetActive(false);
        if (!isPlaying)
        {
            rick.Play();
            isPlaying = true;
        }
    }
}
