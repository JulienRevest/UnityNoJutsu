using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    public ParticleSystem particles;
    public Rigidbody bulletRigidbody;
    private int damage;
    private BulletSounds sound;
    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<BulletSounds>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        // List of elements that should collide with the bullet
        if(collision.gameObject.CompareTag("Ennemy") ||
           collision.gameObject.CompareTag("Player") ||
           collision.gameObject.CompareTag("Wall") ||
           collision.gameObject.CompareTag("EnnemyBullet") ||
           collision.gameObject.CompareTag("PlayerBullet"))
        {
            bulletRigidbody.velocity = Vector3.zero;
            Instantiate(particles, transform.position, Quaternion.LookRotation(Vector3.up));
            //sound.playSound();  // Didn't have time to properly fix the bullet sounds, plus they are quickly annoying
            Destroy(gameObject);
        }
    }

    public void setBulletDamage(int damage)
    {
        this.damage = damage;
    }

    public int getBulletDamage()
    {
        return damage;
    }
}
