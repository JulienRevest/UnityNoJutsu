using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSounds : MonoBehaviour
{
    public AudioClip[] sounds;
    public AudioSource source;

    public void playSound()
    {
        source.clip = sounds[Random.Range(0, sounds.Length)];
        source.Play();
    }
}
