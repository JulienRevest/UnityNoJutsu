using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletParticles : MonoBehaviour
{
    public ParticleSystem particleSystem;

    ParticleSystem.EmissionModule emissionModule;
    ParticleSystem.MainModule mainModule;
    // Start is called before the first frame update
    void Start()
    {
        emissionModule = particleSystem.emission;
        mainModule = particleSystem.main;
    }

    // Update is called once per frame
    void Update()
    {
        emissionModule.rateOverTime = 200.0f;
    }
}
