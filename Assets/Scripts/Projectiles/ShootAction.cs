using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAction : MonoBehaviour
{
    public GameObject BulletPrefab;
    public Transform ShootPoint;
    public float BulletVelocity = 30;

    [Tooltip("Bullet Interval (in ms)")]
    public float BulletInterval = 500;

    public float ReloadCooldown = 3;

    [System.NonSerialized]
    internal float _shotCooldown = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_shotCooldown > 0)
        {
            _shotCooldown -= Time.deltaTime;
        }
    }

    internal void Shoot(string tag, int damage)
    {
        if (_shotCooldown > 0 && tag != "EnnemyBullet") return;

        _shotCooldown = BulletInterval / 1000f;

        GameObject bulletObj = Instantiate(BulletPrefab, ShootPoint.position, ShootPoint.rotation);
        Rigidbody bulletRigidbody = bulletObj.GetComponent<Rigidbody>();

        bulletObj.GetComponent<BulletLogic>().setBulletDamage(damage);
        TrailRenderer trail = bulletObj.GetComponent<TrailRenderer>();

        bulletObj.gameObject.tag = tag;
        switch (tag)
        {
            case "PlayerBullet":
                // Player Bullet layer
                bulletObj.gameObject.layer = 8;
                trail.startColor = Color.green;
                trail.endColor = Color.white;
                break;
            case "EnnemyBullet":
                // Player Bullet layer
                bulletObj.gameObject.layer = 9;
                trail.startColor = Color.red;
                trail.endColor = Color.white;
                break;
        }
        bulletRigidbody.velocity = BulletVelocity * ShootPoint.forward;

        Destroy(bulletObj, 5);
    }

    internal void Reload()
    {
        _shotCooldown = ReloadCooldown;
    }
}
