using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform target;
    public Camera playerCamera;
    public float offset = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        playerCamera.orthographicSize = offset;
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
            transform.position = target.position + new Vector3(0, 10, 0);
    }
}
