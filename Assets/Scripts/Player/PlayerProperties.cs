using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerProperties : MonoBehaviour
{
    public int maxHealth = 10;
    public int currentHealth = 10;
    public GameManager gameManager;

    private bool hasReloaded = false;

    // Update is called once per frame
    void Update()
    {
        if(currentHealth <= 0)
        {
            if(!hasReloaded)
            {
                hasReloaded = true;
                gameManager.Dead();
            }
            Destroy(gameObject, 1);
        }
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnnemyBullet"))
        {
            currentHealth -= collision.gameObject.GetComponent<BulletLogic>().getBulletDamage();
        }
    }
}
