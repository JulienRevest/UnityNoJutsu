using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    public Rigidbody playerRigidbody;
    public GameManager gameManager;
    public float rollSpeed = 7.0f;
    [TooltipAttribute("Duration of the roll (in seconds)")]
    public float rollDuration = 0.5f;
    [TooltipAttribute("Roll cooldown (in seconds)")]
    public float rollCooldown = 2.0f;

    [SerializeField]
    private float _moveSpeed = 5.0f;
    private float moveSpeed;
    private float rollDurationCounter;
    private PlayerProperties properties;
    private Animator animator;

    [System.NonSerialized]
    public float rollCooldownCounter;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = _moveSpeed;
        animator = GetComponent<Animator>();
    }
    void Awake()
    {
        properties = GetComponent<PlayerProperties>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.isPaused)
            return;
        if(properties.currentHealth > 0)
        {
            /** Mouse movement*/
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 0;
            Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);

            mousePos.x = mousePos.x - objectPos.x;
            mousePos.y = mousePos.y - objectPos.y;

            float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, -angle + 90, 0));

            /** Key movement */
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            /** Animation */
            animator.SetFloat("Speed", playerRigidbody.velocity.magnitude);

            /** Roll */
            if (rollDurationCounter > 0)
            {
                rollDurationCounter -= Time.deltaTime;
            } 
            else
            {
                Vector3 movement = new Vector3(horizontal * moveSpeed, 0, vertical * moveSpeed);
                playerRigidbody.velocity = movement;
            }
            if (Input.GetButtonDown("Roll") && rollCooldownCounter <= 0)
            {
                rollCooldownCounter = rollCooldown;
                rollDurationCounter = rollDuration;
                Vector3 movement = new Vector3(playerRigidbody.velocity.x * rollSpeed, 0, playerRigidbody.velocity.z * rollSpeed);
                Physics.IgnoreLayerCollision(6, 9, true);
                playerRigidbody.velocity = movement;
            }
        } else
        {
            playerRigidbody.velocity = new Vector3(0, 0, 0);
            playerRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }

        if(rollCooldownCounter > 0)
        {
            rollCooldownCounter -= Time.deltaTime;
        }
        
        if(rollDurationCounter <= 0)
        {
            Physics.IgnoreLayerCollision(6, 9, false);
        }
    }
}
