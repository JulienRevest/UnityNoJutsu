using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource SFXSource;
    public AudioClip musicClip;
    public AudioClip SFXClip;
    // Start is called before the first frame update
    void Start()
    {
        musicSource.clip = musicClip;
        SFXSource.clip = SFXClip;
        musicSource.loop = true;
        musicSource.volume = 0.5f;
        SFXSource.volume = 0.5f;
        musicSource.Play();
    }

    public void playReload()
    {
        SFXSource.Play();
    }
}
