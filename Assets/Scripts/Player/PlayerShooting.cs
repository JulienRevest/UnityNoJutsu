using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int BulletCount = 10;
    public int MaxBullet = 10;
    public int bulletDamage = 1;
    public GameManager gameManager;

    [System.NonSerialized]
    internal ShootAction _shoot;
    private PlayerSounds sounds;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Awake is called at the start of the scene
    private void Awake()
    {
        _shoot = GetComponent<ShootAction>();
        sounds = GetComponent<PlayerSounds>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.isPaused)
            return;
        if(Input.GetMouseButton(0))
        {
            if (BulletCount <= 0)
            {
                _shoot.Reload();
                BulletCount = MaxBullet;
            }
            else if (BulletCount > 0 && _shoot._shotCooldown <= 0)
            {
                _shoot.Shoot("PlayerBullet", bulletDamage);
                BulletCount--;
            }
        }
        if((Input.GetKeyDown(KeyCode.R) || Input.GetMouseButtonDown(1)) && !(BulletCount >= MaxBullet) && !gameManager.isPaused)
        {
            _shoot.Reload();
            sounds.playReload();
            BulletCount = MaxBullet;
        }
    }
}
