using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject continueButton;
    public GameObject newGameplusButton;
    public void Start()
    {
        if (!PlayerPrefs.HasKey("HasFinishedGame"))
            PlayerPrefs.SetInt("HasFinishedGame", 0);
        // Unused, as I couldn't get the Scene Index to work
        //if (PlayerPrefs.HasKey("CompletedLevel"))
        //    continueButton.SetActive(true);
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Map1");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Continue()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("CompletedLevel"));
    }

    public void NewGamePlus()
    {

    }

    public void Credits()
    {

    }
}
