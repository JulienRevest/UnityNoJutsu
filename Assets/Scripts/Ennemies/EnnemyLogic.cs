using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyLogic : MonoBehaviour
{
    public GameObject playerToLookAt;
    public int maxHealth = 10;
    public GameObject _waypointA;
    public GameObject _waypointB;
    public GameManager gameManager;

    private Animator animator;
    public int currentHealth;

    private bool shouldMove = true;
    private bool moveReverse = false;

    private Vector3 lastPosition;

    [SerializeField]
    private float _moveSpeed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(currentHealth > maxHealth)
            currentHealth = maxHealth;
        Vector3 ennemyPos = transform.position;
        if(playerToLookAt != null)
        {
            Vector3 playerPos = playerToLookAt.transform.position;
            playerPos.y = 0;

            playerPos.x = playerPos.x - ennemyPos.x;
            playerPos.z = playerPos.z - ennemyPos.z;

            float angle = Mathf.Atan2(playerPos.z, playerPos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, -angle + 90, 0));
        }
        if(!shouldMove && Random.Range(0, 10) == 0)
        {
           shouldMove = true;
        }

        if(shouldMove)
        {
            if (moveReverse)
            {
                Vector3 WaypointB = _waypointB.transform.position;
                WaypointB.y = gameObject.transform.position.y;
                transform.position = Vector3.MoveTowards(ennemyPos, WaypointB, _moveSpeed * Time.deltaTime);
            }
            else
            {
                Vector3 WaypointA = _waypointA.transform.position;
                WaypointA.y = gameObject.transform.position.y;
                transform.position = Vector3.MoveTowards(ennemyPos, WaypointA, _moveSpeed * Time.deltaTime);
            }

            if (Mathf.Approximately(ennemyPos.x, _waypointA.transform.position.x) && Mathf.Approximately(ennemyPos.z, _waypointA.transform.position.z))
            {
                moveReverse = true;
                shouldMove = false;
            } else if (Mathf.Approximately(ennemyPos.x, _waypointB.transform.position.x) && Mathf.Approximately(ennemyPos.z, _waypointB.transform.position.z))
            {
                moveReverse = false;
                shouldMove = false;
            }
        }

        Vector3 velocity = (transform.position - lastPosition) / Time.deltaTime;
        lastPosition = transform.position;

        /** Animation */
        animator.SetFloat("Speed", velocity.magnitude);

        if (currentHealth <= 0)
        {
            gameManager.addScore(Vector3.Distance(gameObject.transform.position, playerToLookAt.transform.position));
            Destroy(gameObject);
        }
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("PlayerBullet"))
        {
            currentHealth -= collision.gameObject.GetComponent<BulletLogic>().getBulletDamage();

        }
    }
}
