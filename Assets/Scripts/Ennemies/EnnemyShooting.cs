using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyShooting : MonoBehaviour
{
    [Tooltip("Bullet Interval (in ms)")]
    public float _shootCooldown = 500;
    public GameObject playerToShoot;
    public float shootingDistanceMax = 10.0f;
    public int bulletDamage = 1;

    [System.NonSerialized]
    internal ShootAction _shoot;

    private float coolDown = 500;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Awake is called at the start of the scene
    private void Awake()
    {
        _shoot = GetComponent<ShootAction>();
        coolDown = _shootCooldown;
    }
    // Update is called once per frame
    void Update()
    {
        if(playerToShoot != null && Vector3.Distance(gameObject.transform.position, playerToShoot.transform.position) <= shootingDistanceMax)
        {
            if (_shootCooldown > 0)
            {
                _shootCooldown -= Time.deltaTime * 1000;
            }
            else
            {
                _shoot.Shoot("EnnemyBullet", bulletDamage);
                _shootCooldown = coolDown;
            }
        }
    }
}
