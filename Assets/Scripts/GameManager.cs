using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject pauseMenu;
    public CanvasGroup victoryMenu;
    public string nextSceneName;
    public float victoryFadeDuration = 0.4f;
    public Gameplay gameplayUI;

    private Scene currentScene;
    private string sceneName;
    private bool isFaded = false;
    private int score = 0;

    //[System.NonSerialized]
    public bool isPaused = false;

    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] ennemies = GameObject.FindGameObjectsWithTag("Ennemy");
        int ennemiesLeft = ennemies.Length;
        if(ennemiesLeft <= 0)
        {
            if(!isPaused)
                FinishedLevel();
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    public void loadScene(string SceneName, int SecondsToWait = 0)
    {
        sceneName = SceneName;
        Invoke("sceneInvoke", SecondsToWait);
    }

    private void sceneInvoke()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
    
    public void FinishedLevel()
    {
        victoryMenu.gameObject.SetActive(true);
        isFaded = true;
        isPaused = true;
        /* Attempt at a Continue button, couldn't manage to get the Scene index working
        int completedInt = SceneManager.GetSceneAt(currentScene.buildIndex + 1).buildIndex;
        if (currentScene.buildIndex + 1 > 3)
        {
            completedInt = 3;
        }
        PlayerPrefs.SetInt("CompletedLevel", completedInt);
        */
        Time.timeScale = 0f;
        StartCoroutine(FadeVictoryScreen(victoryMenu, victoryMenu.alpha, isFaded ? 1 : 0));
    }

    private IEnumerator FadeVictoryScreen(CanvasGroup canvasGroup, float start, float end)
    {
        float counter = 0f;

        while(counter < victoryFadeDuration)
        {
            counter += Time.fixedDeltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, counter / victoryFadeDuration);
            yield return null;
        }
    }

    public void NextLevel()
    {
        Time.timeScale = 1f;
        loadScene(nextSceneName);
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        loadScene("MainMenu");
    }

    public void Dead()
    {
        loadScene(SceneManager.GetActiveScene().name, 2);
    }

    public void addScore(float distance)
    {
        score += (int)(distance * 100);
        gameplayUI.scoreText.text = "Score : " + score.ToString();
    }
}
